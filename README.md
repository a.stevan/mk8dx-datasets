# Mario Kart 8 Deluxe - Datesets

This repo contains a few _Mario Kart 8 Deluxe_ datasets.

![mk8dx](assets/mk8dx.png)

## World records

- CSV file: [`world_records.csv`](world_records.csv)
- source: [_Mario Kart 8 Deluxe World Records_](https://mkwrs.com/mk8dx/)
- date: _2024-06-17_
- columns:
    - `Track`: the name of the track
    - `Time`: the total time of the WR, measured by running 3 laps on the track
    - `Hybrid Controls Used`: whether or not _hybrid controls_ have been used or
      not, either `"true"` or `"false"`
    - `Player`: the name of the player who owns the WR
    - `Nation`: the nation where the player owning the WR comes from
    - `Date`: the date at which the WR was recorded
    - `Duration`: an unused column
    - `Character`: the name of the character used in game for the WR
    - `Vehicle`: the name of the vehicle used in game for the WR
    - `Tires`: the name of the tires used in game for the WR
    - `Glider`: the name of the glider used in game for the WR

> :bulb: **Note**
>
> some characters have a color attached to their names, e.g. `Yellow Yoshi`.
>
> However, this does not change the _stats_ at all!
>
> Below is the list of all characters that might have colors:
>   - Yoshi
>   - Birdo
>   - Inkling Boy
>   - Inkling Girl

## In-game combos

> :bulb: **Note**
>
> - the values for each _stat_ range from $0$ to $20$
> - all the datasets in this section use the same set of _stats_
>   - `name` (`str`): the name of the item
>   - `WG` (`int`): **W**ei**G**ht
>   - `AC` (`int`): **AC**celeration
>   - `ON` (`int`): **ON**-road traction
>   - `OF` (`int`): (**OF**f-road) traction
>   - `MT` (`int`): **M**ini-**T**urbo
>   - `SL` (`int`): ground **S**peed
>   - `SW` (`int`): **W**ater **S**peed
>   - `SA` (`int`): **A**nti-gravity **S**peed
>   - `SG` (`int`): air **S**peed
>   - `TL` (`int`): ground handling
>   - `TW` (`int`): **W**ater handling
>   - `TA` (`int`): **A**nti-gravity handling
>   - `TG` (`int`): air handling
>   - `IV` (`int`): **I**n**V**incibility

### Characters

- CSV file: [`characters.csv`](characters.csv)
- source: [_Mario Kart 8 Deluxe in-game statistics_][drivers stats]
- version: `3.0.0`
- date: _2024-10-15_
- columns: see note above

### Vehicles

- CSV file: [`vehicles.csv`](vehicles.csv)
- source: [_Mario Kart 8 Deluxe in-game statistics_][bodies stats]
- version: `3.0.0`
- date: _2024-10-15_
- columns: see note above

### Tires

- CSV file: [`tires.csv`](tires.csv)
- source: [_Mario Kart 8 Deluxe in-game statistics_][tires stats]
- version: `3.0.0`
- date: _2024-10-15_
- columns: see note above

### Gliders

- CSV file: [`gliders.csv`](gliders.csv)
- source: [_Mario Kart 8 Deluxe in-game statistics_][gliders stats]
- version: `3.0.0`
- date: _2024-10-15_
- columns: see note above

## Playing with the datasets

I wrote some [Nushell](https://www.nushell.sh/) commands to query the datasets and wanted to share
some cool things.

First, let's load the module:
```bash
use mod.nu *
```

Then, the datasets can be loaded:
```bash
let wrs = open world_records.csv
let combos = load_mk8_combos_dataset '.'
```

The combos can be queried, e.g. to get the stats of the current _online meta_:
```bash
$combos | get combo {
    character: "Yoshi",
    vehicle: "Teddy Buggy",
    tires: "Azure Roller",
    glider: "Paper Glider",
}
```

We can also get the unique classes of combo parts with `group-by-item-class`:
```bash
$combos.characters | group-by-item-class
```

The following will extract the _stats_ for all the world records, allowing to plot them for instance:
```bash
let wrs_stats = $wrs
    | rename --column {
        Character: "character", Vehicle: "vehicle", Tires: "tires", Glider: "glider"
    }
    | update character {
        if ($in | str contains "Yoshi") {
            "Yoshi"
        } else if ($in | str contains "Birdo") {
            "Birdo"
        } else if ($in | str contains "Inkling Boy") {
            "Inkling Boy"
        } else if ($in | str contains "Inkling Girl") {
            "Inkling Girl"
        } else {
            $in
        }
    }
    | uniq
    | insert stats { |it| $combos | get combo $it }
    | select Track character vehicle tires glider stats
    | flatten
```

And finally, using [GPLT](https://gitlab.isae-supaero.fr/a.stevan/gplt), we can plot the _stats_ of
the world records:
```bash
use std repeat

def extract-stat-values [stat: cell-path]: [
    nothing -> record<name: string, points: table<x: float, y: float>>
] {
    {
        name: $stat,
        points: (
            $in
                | group-by $stat
                | transpose x y
                | update y { length }
                | into int x y
                | sort-by x
                | reduce --fold (0 | repeat 21) { |it, acc| $acc | update $it.x $it.y }
                | enumerate
                | rename --column { index: x, item: y }
        ),
    }

}

let graphs = [
    ($wrs_stats | extract-stat-values $.MT),
    ($wrs_stats | extract-stat-values $.SL),
    ($wrs_stats | extract-stat-values $.WG),
    ($wrs_stats | extract-stat-values $.IV),
    ($wrs_stats | extract-stat-values $.AC),
    ($wrs_stats | extract-stat-values $.TL),
]

$graphs
    | to json
    | gplt plot $in ...[
        --x-label "stat"
        --y-label "frequency"
        --title "Distribution of common stats in MK8 world records"
    ]
```

![wrs_stats](assets/wrs_stats.png)

[drivers stats]: https://www.mariowiki.com/Mario_Kart_8_Deluxe_in-game_statistics#Drivers.27_statistics_.28DV.29
[bodies stats]: https://www.mariowiki.com/Mario_Kart_8_Deluxe_in-game_statistics#Bodies.27_statistics_.28BD.29
[tires stats]: https://www.mariowiki.com/Mario_Kart_8_Deluxe_in-game_statistics#Tires.27_statistics_.28TR.29
[gliders stats]: https://www.mariowiki.com/Mario_Kart_8_Deluxe_in-game_statistics#Gliders.27_statistics_.28WG.29
