export def load_mk8_combos_dataset [dataset_dir: path = '.']: [
    nothing -> record<
        characters: table,
        vehicles: table,
        tires: table,
        gliders: table,
    >
] {
    let dataset_dir = $dataset_dir | path expand

    def load [filename: string, --trim: string] {
        $dataset_dir | path join $filename | open $in | str trim name
    }

    {
        tires: (load tires.csv),
        gliders: (load gliders.csv),
        characters: (load characters.csv),
        vehicles: (load vehicles.csv),
    }
}

export def "get combo" [ combo: record ]: [
    record< characters: table, vehicles: table, tires: table, gliders: table> -> record
] {
    let character = $in.characters | where name == $combo.character | into record | reject -i name
    let vehicle = $in.vehicles | where name == $combo.vehicle | into record | reject -i name
    let tires = $in.tires | where name == $combo.tires | into record | reject -i name
    let glider = $in.gliders | where name == $combo.glider | into record | reject -i name

    if $character == {} {
        error make { msg: $"aie character ($combo)" }
    }
    if $vehicle == {} {
        error make { msg: $"aie vehicle ($combo)" }
    }
    if $tires == {} {
        error make { msg: $"aie tires ($combo)" }
    }
    if $glider == {} {
        error make { msg: $"aie glider ($combo)" }
    }

    {
        WG: ($character.WG + $vehicle.WG + $tires.WG + $glider.WG),
        AC: ($character.AC + $vehicle.AC + $tires.AC + $glider.AC),
        ON: ($character.ON + $vehicle.ON + $tires.ON + $glider.ON),
        OF: ($character.OF + $vehicle.OF + $tires.OF + $glider.OF),
        MT: ($character.MT + $vehicle.MT + $tires.MT + $glider.MT),
        SL: ($character.SL + $vehicle.SL + $tires.SL + $glider.SL),
        SW: ($character.SW + $vehicle.SW + $tires.SW + $glider.SW),
        SA: ($character.SA + $vehicle.SA + $tires.SA + $glider.SA),
        SG: ($character.SG + $vehicle.SG + $tires.SG + $glider.SG),
        TL: ($character.TL + $vehicle.TL + $tires.TL + $glider.TL),
        TW: ($character.TW + $vehicle.TW + $tires.TW + $glider.TW),
        TA: ($character.TA + $vehicle.TA + $tires.TA + $glider.TA),
        TG: ($character.TG + $vehicle.TG + $tires.TG + $glider.TG),
        IV: ($character.IV + $vehicle.IV + $tires.IV + $glider.IV),
    }
}

export def group-by-item-class []: [ table -> table ] {
    group-by { reject name | values | str join '-' }
        | transpose k v
        | get v
        | each { {
            class: ($in.name | sort | first),
            stats: ($in.0 | reject name),
            _: { alternatives: ($in.name | sort | skip 1) },
        } }
        | flatten
}
